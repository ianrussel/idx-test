<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class StudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
         //return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dt     = new Carbon();
        $before = $dt->subYears(13)->format('Y-m-d'); 
        return [
            'firstname'    => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:10',
            'lastname'     => 'required|alpha|min:2|max:10',
            'bday'         => 'required|date|before:' . $before,
            'address'      => 'required|min:10',
            'zip'          => 'required|digits:4',
            'phone'        => 'required|digits:7',
            'mobile'       => 'required|digits:11',
            'email'        => 'required|email',
            'city_id'      => 'required',
            'yearlevel_id' => 'required',
            'section_id'   => 'required',
        ];
    }
}
