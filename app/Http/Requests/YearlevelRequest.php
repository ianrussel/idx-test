<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class YearlevelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
         //return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      
        return [
            'yearlevel' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:20|unique:yearlevels',
        ];
    }
}
