<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
         //return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|regex:/^[\pL\s\-\]+$/u|min:2|max:20',
            'display_name'  => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:20',
            'description'   => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:200'
        ];
    }
}
