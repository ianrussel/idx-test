<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\CityRepository;
use App\Repositories\YearlevelRepository;
use App\Repositories\StudentRepository;
use App\Repositories\SectionRepository;
use Datatables;
use App\Models\Student;
#use App\Models\Permission;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use App\User;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\PermissionRequest;
use App\Http\Requests\AttachPermissionRequest;
/**
 * @author [lito lapidpid] <[<talosenador@yahoo.com>]>
 */
class AdminController extends Controller
{
	protected $cities;
	protected $yearlevels;
	protected $students;
    protected $sections;

	public function __construct(CityRepository $cities, YearlevelRepository $yearlevels, StudentRepository $students, SectionRepository $sections)
	{
		$this->cities     = $cities;
		$this->yearlevels = $yearlevels;
		$this->students   = $students;
        $this->sections   = $sections;
 	}
    public function getIndex()
    {
    	/*$cities     = $this->cities->getAllCreatedCities();
    	$yearlevels = $this->yearlevels->getAllYearLevels();
    	$students   = $this->students->getAllStudentsGroupByLevelAndSection();
    	return view('admin.admin_tabs', array(
    		'cities'     => $cities,
    		'yearlevels' => $yearlevels,
    		'students'   => $students
    	));*/
    	return view('datatables.index');
    }

    public function anyData()
    {
    	$students = Student::with(array('section','yearlevel'))->select('students.*');
    	return Datatables::of($students)
    		->setRowClass(function($student) {
    			return $student->id % 2 === 0 ? 'alert-success' : 'alert-warning';
    		})
    		->make(true);
    }

    public function searchByAge(Request $request)
    {
        /**
         * age does not matter
         * @author FPJ(Filipi Piput Jhabongga) <[ian.russel@yahoo.com]>
         */
        $min      = $request->input('age_from',15);//set initial value else laravel wil complain something is null
        $max      = $request->input('age_to', 100);//same here
     
        $students = $this->students->getStudentsByAge($min, $max);
 
        return view('admin.age_does_not_matter', array(
            'students' => $students,
            'ageFrom'  => $min,
            'ageTo'    => $max
        ));
    }

    public function groupByLevelAndSection($level, $section)
    {
        /**
         * @author RRD(RobredoRoxasRobin) <[<ian.russel@yahoo.com>]>
         */
    }

    public function searchByWhatEver(Request $request)
    {
        /**
         *
         * @var array
         * @author MR.B(Bibingka Ni Binay) <[ian.russel@yahoo.com]>
         */
        $Allcities     = $this->cities    ->getAllCreatedCities();
        $Allyearlevels = $this->yearlevels->getAllYearLevels();
        $Allsections   = $this->sections  ->getAllSections();
     
        $city          = $request->input('city_id',10);
        $section       = $request->input('section_id', 5);
        $yearlevel     = $request->input('yearlevel_id',7);

        $searchResults = $this->students->searchByWhatEver($city, $section, $yearlevel);
        //dd($searchResults);
        //return response('success','no');
        return view('admin.search_by_whatever', array(
            'Allcities'     => $Allcities,
            'Allyearlevels' => $Allyearlevels,
            'Allsections'   => $Allsections,
            'Allstudents'   => $searchResults
        ));
    }

    public function index()
    {
        $users = User::all();

        return view('admin.all_users_lists', array(
            'allUsers' => $users
        ));
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 403);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function allRoles(Request $request)
    {
        $allRoles = Role::all();

        return view('admin.role', array(
            'allRoles' => $allRoles
        ));
    }
    public function createRole(RoleRequest $request)
    {
        $username           = \Auth::user()->name;//yeah boy
        $role               = new Role;
        $role->name         = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->description  = $request->input('description');
        $role->created_by   = $username;
        $role->save();

        return redirect('/idx-test/allroles')->with('success', 'New Role Added');
    }
    public function allPermissions(Request $request)
    {
        $allPermissions = Permission::all();

        return view('admin.permission', array(
            'allPermissions' => $allPermissions
        ));
    }

    public function createPermission(PermissionRequest $request)
    {
        $username                = \Auth::user()->name;
        $viewUsers               = new Permission();
        $viewUsers->name         = $request->input('name');
        $viewUsers->display_name = $request->input('display_name');
        $viewUsers->description  = $request->input('description');
        $viewUsers->created_by   = $username;
        $viewUsers->save();

        return redirect('/idx-test/allpermissions')->with('success', 'New Permission Added');
    }

    public function displayAssignRole(Request $request)
    {
        $users = User::all();
        $roles = Role::all();

        return view('admin.assignrole', array(
            'allRoles' => $roles,
            'allUsers' => $users
        ));
    }

    public function assignRole(Request $request)
    {
        $user = User::where('email', '=', $request->input('email'))->first();
        $role = Role::where('name', '=', $request->input('role'))->first();
        $user->roles()->attach($role->id);
        return redirect('/idx-test/show-assign-roles-form')->with("success","Role Assigned". ' to ' . $user->name);
    }

    public function displayAttachPermissionToRole(Request $request)
    {
        $permission = Permission::all();
        $roles      = Role::all();

        return view('admin.attach_permission_to_role', array(
            'allRoles'       => $roles,
            'allPermissions' => $permission
        ));
    }

    public function attachPermission(AttachPermissionRequest $request)
    {
        $role = Role::where('name', '=', $request->input('role'))->first();
        $permission = Permission::where('name', '=', $request->input('permission'))->first();
        $permission->roles()->attach($role->id);

        return redirect('/idx-test/display_attach_permission_to_role')->with("success", "Permission " . $permission->name . ' is added to ' . $role->name );
    }
    public function showUser($id)
    {
        $user = User::findOrFail($id);

        return view('admin.edit_users', array(
            'user' => $user
        ));
    }
    public function updateUser(Request $request, $id)
    {
        
        $user           = User::findOrFail($id);
        $input          = $request->all();
        $user->isActive = $input['isActive'];
  
        $user->save();
        return redirect('idx-test/api/users')->with('success',$user->name. ' status is updated!');
    }
}
