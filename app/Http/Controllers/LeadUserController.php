<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\LeadUser;
use Datatables;
use App\Repositories\LeadUserRepository;
use DB;

class LeadUserController extends Controller
{
    protected $countspercity;

    public function __construct(LeadUserRepository $countspercity)
    {
        $this->countspercity = $countspercity;
        $this->middleware('auth');
    }

    public function index()
    {
    	///$lead_users    = LeadUser::all();
    	$lead_users = LeadUser::limit(30)->offset(30)->get();

    	//dd($lead_users);
    	return view('leads.all_leads', array(
    		'all_leads' => $lead_users,      
    	));
    }

    public function getIndex(Request $request)
    {
    	return view('leads.datatables_for_leads');
    }

    public function getDataFromDatatables(Request $request)
    {
        $leads = LeadUser::select(['*']);

        return Datatables::of($leads)
            ->filter(function ($instance) use ($request) {
                if ($fname =$request->first_name) {
                    $instance->where('first_name','like',"%$fname");
                }
                
                if ($lname = $request->last_name) {
                   $instance->where('last_name','like',"%$lname");
                }
            })
            ->addColumn('fullname', function ($query) {
                return $query->getFullName();
            })
            ->addColumn('mobile', function ($query) {
                return $query->yesOrNo();
            })
            ->addColumn('age', function ($query) {
                return $query->getAge();
            })
             ->editColumn('birthdate', function ($query) {
                return $query->birthdate->toFormattedDateString();
            })
            ->make(true);
    }
    public function countPerCity(Request $request)
    {
        $cities = $this->countspercity->getAllCities();

        $city   = $request->input('city');

        $counts = $this->countspercity->getLeadsPerCity($city); 
        $leadcounts = $this->countspercity->countsAllLeadsPerCity($city); 
            return view('leads.counts_by_city', array(
                'counts' => $counts,
                'cities' => $cities,
                'leadcounts' => $leadcounts,
                'city'   => $city
            ));
    }

    public function autocompleteSearch(Request $request)
    {
        $city = $request->term;
        $results = array();

        $queries = LeadUser::select('city')
                    ->where('city', 'LIKE', '%'.$city.'%')
                    ->take(10)
                    ->get();

        foreach ($queries  as $query) {
            $results[] = ['value' => $query->city];
        }
        return \Response::json($results);
    }

    public function countPerState(Request $request)
    {
       $states = LeadUser::distinct()
            ->take(10)
            ->get(['state']);
        $state   = $request->input('state');
        $allLeadsCount    = $this->countspercity->countAllLeadsPerState($state);
        $allLeadsPerState = $this->countspercity->getAllLeadsPerState($state);

        return view ('leads.counts_by_state', compact('states','state','allLeadsCount','allLeadsPerState'));
    }
    public function autoCompletePerState(Request $request)
    {
        $state = $request->term;
        $states = LeadUser::where('state', 'LIKE', '%'.$state.'%')
                    ->distinct()
                    ->take(10)
                    ->get(['state']);
        return $states;
    }

    public function countPerGender(Request $request)
    {
        $gender = $request->input('gender');
        $leads = LeadUser::where('gender', 'LIKE', '%'.$gender.'%')
                ->orderBy('first_name', 'ASC')
                ->paginate(20);
        $sex = ($gender == 'M' ? 'Male' : 'Female');
        
        return view('leads.counts_by_gender', compact('leads','sex','gender_count'));
    }
}
