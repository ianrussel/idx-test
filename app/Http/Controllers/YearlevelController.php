<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\YearlevelRepository;
use App\Models\Yearlevel;
use App\Http\Requests\YearlevelRequest;
/**
 * @author  <[<email address>]>
 */

class YearlevelController extends Controller
{
    /**
     * use custom repository 
     */
    
    protected $yearlevels;

    public function __construct(YearlevelRepository $yearlevels)
    {
    	$this->yearlevels = $yearlevels;
<<<<<<< HEAD
        $this->middleware('auth');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
    }

    public function index(Request $request)
    {
    	$allYearlevels = $this->yearlevels->getAllCreatedYearLevels();

    	return view('yearlevels.all_year_levels', array(
    		'allYearlevels' => $allYearlevels
    	));
    }

    public function addNewYearLevel(YearlevelRequest $request)
    {

        $username              = \Auth::user()->name;
        $input                 = $request->all();
        $yearlevel             = new Yearlevel;
        $yearlevel->yearlevel  = $input['yearlevel'];
        $yearlevel->created_by = $username;

        $yearlevel->save();

        return redirect('/idx-test/year-levels')->with('success', 'New Year level Added');
    }
}
