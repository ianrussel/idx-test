<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\City;
use App\Models\State;
use App\Repositories\CityRepository;
use App\Http\Requests\CityRequest;

class CityController extends Controller
{
    /**
     * for reusability, we use custom repository instead of 
     * querying directly from this controller
     * we can use this later anywhere in controllers 
     * this idea was fetch from Symfony Doctrine :)
     */
    
    protected $cities;

    public function __construct(CityRepository $cities)
    {
    	//dependency injection
    	$this->cities = $cities;
<<<<<<< HEAD
        $this->middleware('auth');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
    }

    public function index(Request $request)
    {
    	$allCities = $this->cities->getAllCreatedCities();
        $states    = State::all();

    	return view('cities.all_cities', array(
    		'allCities' => $allCities,
            'states'    => $states,
    	));
    }

    public function addNewCity(CityRequest $request)
    {
        
        $username              = \Auth::user()->name;
        $input                 = $request->all();
        $city                  = new City;
        $city->city            = $input['city'];
        $city->state_id        = $input['state_id'];
        $city->created_by      = $username;

        $city->save();

        return redirect('/idx-test/cities')->with('success', 'New City Added');
    }
}
