<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Student;
use App\Repositories\StudentRepository;
use App\Repositories\CityRepository;
use App\Repositories\SectionRepository;
use App\Repositories\YearlevelRepository;
use App\Http\Requests\StudentRequest;
use App\Http\Controllers\AuthController;
<<<<<<< HEAD
//use Crypt;
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f

/**
 * @author james bond <[<email address>]>
 */

class StudentController extends Controller
{
    /**
     * use custom repositories, a great idea from Fabien Potencier aka founder of Symfony.com
     * let the bulk of data access logic handled by repository
     * also for future reusability e.g json response
     */
    
    protected $students;
    protected $cities;
    protected $sections;
    protected $yearlevels;
  

    /**
     * injecting the repositories
     * @param StudentRepository   $students   [description]
     * @param CityRepository      $cities     [description]
     * @param SectionRepository   $sections   [description]
     * @param YearlevelRepository $yearlevels [description]
     */
    public function __construct(StudentRepository $students, CityRepository $cities, SectionRepository $sections, YearlevelRepository $yearlevels)
    {
    	//laravel's dependency injection?
    	$this->students   = $students;
        $this->cities     = $cities;
        $this->sections   = $sections;
        $this->yearlevels = $yearlevels;
<<<<<<< HEAD
        $this->middleware('auth');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
    }

    public function index(Request $request)
    {
    	
        $allStudents   = $this->students  ->getAllCreatedStudents();
        $allCities     = $this->cities    ->getAllCities();
        $allSections   = $this->sections  ->getAllSections();
        $allYearlevels = $this->yearlevels->getAllYearlevels();
<<<<<<< HEAD
        //s$crypted = Crypt::encrypt($allYearlevels);
        
=======

>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
    	return view('students.all_students', array(
    		'allStudents'   => $allStudents,
            'allCities'     => $allCities,
            'allSections'   => $allSections,
            'allYearlevels' => $allYearlevels
    	));
    }

    public function addNewStudent(StudentRequest $request)
    {
        /**
         * maintain controller "thin"
         * let the validation handled somewhere else
         */
        $username              = \Auth::user()->name;
        $input                 = $request->all();
        $student               = new Student;
        $student->firstname    = $input['firstname'];
        $student->lastname     = $input['lastname'];
        $student->bday         = $input['bday'];
        $student->address      = $input['address'];
        $student->zip          = $input['zip'];
        $student->phone        = $input['phone'];
        $student->mobile       = $input['mobile'];
        $student->email        = $input['email'];
        $student->city_id      = $input['city_id'];
        $student->yearlevel_id = $input['yearlevel_id'];
        $student->section_id   = $input['section_id'];
        $student->created_by   = $username;
        $student->age          = $this->howOldIsYou(new \DateTime('now'), new \DateTime($input['bday']));
        $student->save();

        return redirect('idx-test/all-students')->with('success', 'Student Created!');
    }

    public function showStudent($id)
    {
        $allCities     = $this->cities    ->getAllCities();
        $allSections   = $this->sections  ->getAllSections();
        $allYearlevels = $this->yearlevels->getAllYearlevels();
        $student       = Student::findOrFail($id);

        return view('students.showIndividualStudent', array(
            'student'       => $student,
            'allCities'     => $allCities,
            'allSections'   => $allSections,
            'allYearlevels' => $allYearlevels
        ));
    }

    public function updateThisStudent(StudentRequest $request, $id)
    {
        
        $student               = Student::findOrFail($id);
        $input                 = $request->all();
        $username              = \Auth::user()->name;
        $student->firstname    = $input['firstname'];
        $student->lastname     = $input['lastname'];
        $student->bday         = $input['bday'];
        $student->address      = $input['address'];
        $student->zip          = $input['zip'];
        $student->phone        = $input['phone'];
        $student->mobile       = $input['mobile'];
        $student->email        = $input['email'];
        $student->city_id      = $input['city_id'];
        $student->yearlevel_id = $input['yearlevel_id'];
        $student->section_id   = $input['section_id'];
        $student->updated_by   = $username;
        $student->age          = $this->howOldIsYou(new \DateTime('now'), new \DateTime($input['bday']));
        $student->save();
        return redirect('idx-test/show-student/'.$student->id)->with('success',$student->getFullname(). ' profile is updated!');
    }

    protected function howOldIsYou($now, $birthday)
    {
        /**
         * calculate Age
         * @var date
         */
        $age = $now->diff($birthday)->format("%y");
        return $age;
    }

    public function destroy($id)
    {
        /**
         * kill the bastard student
         * [$student description]
         * @var mixed
         */
        $student = Student::findOrFail($id);
        $student->delete();

        return redirect('idx-test/all-students')->with('success', ' He is dead');
    }
}
