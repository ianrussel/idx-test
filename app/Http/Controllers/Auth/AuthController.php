<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
<<<<<<< HEAD
use Socialite;
use Illuminate\Http\Request;
use App\SocialAccountService;
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/idx-test/all-students';

  
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'created_by' => "Himself"
        ]);
    }
<<<<<<< HEAD
    /**
    * Redirect user to the Facebook authentication page
    */
    public function redirectToProvider(Request $request) 
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
    * Obtain the user information from Facebook
    * return Response
    */

    public function handleProviderCallback(SocialAccountService $service)
    {
         $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
         auth()->login($user);
        return redirect()->to('idx-test/lead-users');
    }
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
}
