<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Section;
use App\Repositories\SectionRepository;
use App\Http\Requests\SectionRequest;
/**
 * @author   <amaybisayaatyahoodotcom>
 */
class SectionController extends Controller
{
    /**
     * use custom repository instead of directly query in this controller
     * for reusability, e.g sending response for Ajax requests 
     */
    
    protected $sections;

    public function __construct(SectionRepository $sections)//we allow type hinting here
    {
    	$this->sections = $sections;
<<<<<<< HEAD
        $this->middleware('auth');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
    }

    public function index(Request $request)
    {
    	$allSections = $this->sections->getAllCreatedSections();

    	return view('sections.all_sections', array(
    		'allSections' => $allSections
    	));
    }

    public function addNewSection(SectionRequest $request)
    {
        
        $username              = \Auth::user()->name;
        $input                 = $request->all();
        $section               = new Section;
        $section->section      = $input['section'];
        $section->created_by   = $username;

        $section->save();

        return redirect('/idx-test/student-sections')->with('success', $section->section . ' is Added');
    }
}
