<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Khill\Lavacharts\Lavacharts;
use App\Models\LeadUser;
use App\Http\Requests;

class LeadUserChartController extends Controller
{
   	//$lava = new Lavacharts; // See note below for Laravel
	public function leadUsersChart()
	{
		$population = \Lava::DataTable();
		
		$lead_users = LeadUser::selectRaw('city, count(city) as count')->groupBy('city')->get();
		
		$population->addStringColumn('Year')
           ->addNumberColumn('Number of People');


		foreach ($lead_users->chunk(500) as $chunk) {
			foreach($chunk as $user) {
				$population->addRow([$user->city,$user->count]);
			}
		}
		\Lava::AreaChart('Population', $population, [
		    'title' => 'Lead Users Per City',
		    
		    'legend' => [
		    	'position' => 'in'
		    ]
		]);

		return view('charts.lead_users_chart');
	}
}
