<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\StateRepository;
use App\Models\State;
use App\Http\Requests\StateRequest;

class StateController extends Controller
{
	/**
	 * here I prefer to create custom repository
	 * make it available to other controller,maybe we can reuse 
	 * it later
	 */
	protected $states;

	public function __construct(StateRepository $states)
	{
		$this->states = $states;
<<<<<<< HEAD
        $this->middleware('auth');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
	}

    public function index(Request $request)
    {
    	$allStates = $this->states->getAllCreatedStates();

    	return view('states.all_states', array(
    		'allStates' => $allStates
    	));
    }


    public function addNewState(StateRequest $request)
    {

        $username          = \Auth::user()->name;
        $input             = $request->all();
        $state             = new State;
        $state->state      = $input['state'];
        $state->created_by = $username;

        $state->save();

        return redirect('/idx-test/states')->with('success', $state->state.'/s  Added!');
    }
}
