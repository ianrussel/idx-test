<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::auth();

Route::get('/home', 'HomeController@index');


//states

Route::get('/idx-test/states', 'StateController@index');
Route::post('/idx-test/states/add-new-state', 'StateController@addNewState');

//cities

Route::get('/idx-test/cities', 'CityController@index');
Route::post('/idx-test/add-new-city', 'CityController@addNewCity');
//section

Route::get('/idx-test/student-sections', 'SectionController@index');
Route::post('/idx-test/section/add-new-sections', 'SectionController@addNewSection');

//yearlevels

Route::get('/idx-test/year-levels', 'YearlevelController@index');
Route::post('/idx-test/year-level/add-new-year-levels', 'YearlevelController@addNewYearLevel');

//students

Route::get('/idx-test/all-students', 'StudentController@index');
Route::post('/idx-test/add-new-students', 'StudentController@addNewStudent');
Route::get('/idx-test/show-student/{id}', 'StudentController@showStudent');
Route::patch('/idx-test/update/update-this-student/{id}', 'StudentController@updateThisStudent');
Route::delete('/idx-test/delete/delete-this-student/{id}', 'StudentController@destroy');

//admin routes
//Route::get('/idx-test/admin-area/all-tabs', 'AdminController@index');
Route::controller('datatables', 'AdminController', [
	'anyData'   =>  'datatables.data',
	'getIndex'  =>  'datatables',
]);

Route::get('/idx-test/age/search-by-age', 'AdminController@searchByAge');

Route::get('/idx-test/level/section/city/zip/whatever', 'AdminController@searchByWhatEver');

//create role
Route::post('/idx-test/roles', 'AdminController@createRole');
Route::post('idx-test/permissions', 'AdminController@createPermission');
Route::post('/idx-test/assign-roles', 'AdminController@assignRole');
Route::post('/idx-test/attach-permissions', 'AdminController@attachPermission');

/*Route::group(['prefix' => 'api', 'middleware' => ['ability:admin,create-users']],function() {
	Route::get('users', 'AdminController@index');
});*/

Route::get('/idx-test/api/users', 'AdminController@index');

Route::post('authenticate','AdminController@authenticate');

Route::get('/idx-test/allroles', 'AdminController@allRoles');

Route::get('/idx-test/display_attach_permission_to_role', 'AdminController@displayAttachPermissionToRole');
Route::get('/idx-test/show-assign-roles-form', 'AdminController@displayAssignRole');

Route::get('/idx-test/allpermissions', 'AdminController@allPermissions');

Route::get('/idx-test/show-user/{id}', 'AdminController@showUser');

Route::patch('/idx-test/edit-user/{id}', 'AdminController@updateUser');
<<<<<<< HEAD

//lead_users

Route::get('idx-test/lead-users', 'LeadUserController@index');

//LEAD USERS AJAX

Route::get('lead-users/data', ['uses' => 'LeadUserController@getDataFromDatatables', 'as' => 'lead-users.data']);
Route::get('lead-users', ['uses' => 'LeadUserController@getIndex', 'as' => 'lead-users']);

//jquery and etc
Route::get('lead-users/count-per-city', 'LeadUserController@countPerCity');

Route::get('lead-users/autocomplete', 'LeadUserController@autocompleteSearch');

Route::get('lead-users/count-per-state', 'LeadUserController@countPerState');

Route::get('lead-users/states-autocomplete', 'LeadUserController@autoCompletePerState');

Route::get('lead-users/count-lead-by-gender', 'LeadUserController@countPerGender');

//social network authentications

Route::get('lead-users/auth/facebook', 'Auth\AuthController@redirectToProvider');

Route::get('lead-users/auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

//lava charts

Route::get('lead-users/lead-users/lava/lead-users-chart', 'LeadUserChartController@leadUsersChart');

Route::get('test', function () {
 $data = App\Models\LeadUser::selectRaw('city, count(city) as count')->groupBy('city')->get();
	return view('test')->withData($data);
});

Route::get('lead-users/test-javascript', 'TestController@index');
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
