<?php

namespace App\Listeners;

use App\Events\TaskIsCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskIsCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskIsCompleted  $event
     * @return void
     */
    public function handle(TaskIsCompleted $event)
    {
        //
    }
}
