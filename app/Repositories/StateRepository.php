<?php

namespace App\Repositories;

use App\Models\State;


class StateRepository
{
	public function getAllCreatedStates()
	{
		return $allStates = State::orderBy('id','DESC')->paginate(10);
	}
}