<?php

namespace App\Repositories;

use App\Models\City;

class CityRepository
{
	public function getAllCreatedCities()
	{
		$allCities = City::orderBy('id', 'DESC')->paginate(5);

		return $allCities;
	}

	public function getAllCities()
	{
		$allCities = City::all();

		return $allCities;
	}
}