<?php

namespace App\Repositories;

use App\Models\Yearlevel;
/**
 * @author  <[<email address>]>
 */
class YearlevelRepository
{
	private $allyearlevels;

	public function getAllCreatedYearLevels()
	{
		$this->allyearlevels = Yearlevel::orderBy('id', 'DESC')->paginate(5);

		return $this->allyearlevels;
	}

	public function getAllYearlevels()
	{
		$this->allyearlevels = Yearlevel::all();

		return $this->allyearlevels;
	}
}