<?php

namespace App\Repositories;

use App\Models\LeadUser;
use DB;
class LeadUserRepository
{
	private $counts;

	private $cities;

	public function getLeadsPerCity($city)
	{
		$this->counts = LeadUser::select('*')
		              ->where('city',$city)
		              ->paginate(10);

		return $this->counts->appends(['city' => $city]);
	}

	public function getAllCities()
	{
		return \Cache::remember('cities',5, function () {
			return LeadUser::select('city')
						->orderBy('city', 'DESC')
		                ->distinct()
						->get();
		});
		
	}

	public function countsAllLeadsPerCity($city)
	{
		$this->counts = DB::table('lead_users')
		              ->where('city', $city)
		              ->count();
		return $this->counts;
	}

	public function countAllLeadsPerState($state)
	{
		$this->counts = LeadUser::where('state', $state)
		              ->count();
		return $this->counts;
	}

	public function getAllLeadsPerState($state)
	{
		$this->counts = LeadUser::select('*')
		              ->where('state', $state)
		              ->paginate(10);
		              
		return $this->counts->appends(['state' => $state]);
	}
	public function getAllStates()
	{
		return \Cache::remember('states',5, function () {
			return LeadUser::select('state')
						->orderBy('state', 'DESC')
		                ->distinct()
						->get();
		});
		
	}
}