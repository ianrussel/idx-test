<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\State;

class City extends Model
{
    protected $fillable =['city'];

    public function students()
    {
    	return $this->hasMany(Student::class);
    }

    public function state()
    {
    	return $this->belongsTo(State::class);
    }
}
