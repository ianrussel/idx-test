<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Yearlevel;
use App\Models\City;
use App\Models\Section;

class Student extends Model
{
    protected $fillable = ['firstname', 'lastname', 'address', 'bday','zip', 'phone', 'mobile', 'email', 'section_id', 'yearlevel_id', 'city_id'];

   // private $date = date("Y-m-d");

    public function yearlevel()
    {
    	return $this->belongsTo(Yearlevel::class);
    }

    public function city()
    {
    	return $this->belongsTo(City::class);
    }

    public function section()
    {
    	return $this->belongsTo(Section::class);
    }
    public function getAge()
    {
        if (!$this->bday) {
            return "";// put here what you want if no birthdayprovided
        }
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->bday);
        return $now->diff($birthday)->format("%y");
    }

    public function getFullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
    public function getStudentId()
    {
        return date("Y"). ' - ' . $this->id;
    }
    protected function getCity()
    {
        return $this->city->city;
    }
    protected function getState()
    {
        return $this->city->state->state;
    }
    public function getAddress()
    {
        return $this->address . ' ,' . $this->getCity() . ' ,' . $this->getState();
    }
}
