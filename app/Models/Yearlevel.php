<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;

class Yearlevel extends Model
{
    protected $fillable = ['yearlevel'];

    public function students()
    {
    	return $this->hasMany(Student::class);
    }
}
