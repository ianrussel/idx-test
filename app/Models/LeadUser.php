<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LeadUser extends Model
{
	protected $dates = ['birthdate', 'created_at', 'updated_at'];

    public function getFullName()
    {
    	return $this->first_name . ' ' . $this->last_name;
    }

    public function yesOrNo()
    {
    	if ($this->is_mobile == 1) {
    		$this->is_mobile = 'Yes';
    	} else {
    		$this->is_mobile = 'No';
    	}

    	return $this->is_mobile;
    }
    public function getAge()
    {
    	if (!$this->birthdate) {
            return "";// put here what you want if no birthdayprovided
        }
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->birthdate);
        return $now->diff($birthday)->format("%y");
    }
    public function getGender()
    {
        return $this->gender == 'M' ? 'Male' : 'Female';
    }
}
