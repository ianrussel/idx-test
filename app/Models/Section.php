<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;

class Section extends Model
{
    protected $fillable = ['section'];

    public function students()
    {
    	return $this->hasMany(Student::class);
    }
}
