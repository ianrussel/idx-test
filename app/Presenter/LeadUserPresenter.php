<?php

namespace App\Presenter;

use Laracasts\Presenter\Presenter;

class LeadUserPresenter extends Presenter {

    public function fullName()
    {
    	return $this->first_name . ' ' . $this->last_name;
    }

    public function isMobile()
    {
    	if ($this->is_mobile == 1) {
    		$this->is_mobile = 'Yes';
    	} else {
    		$this->is_mobile = 'No';
    	}

    	return $this->is_mobile;
    }
    public function birthdate()
    {
        if (!$this->entity->birthdate) {
            return "";// put here what you want if no birthdayprovided
        }
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->entity->birthdate);
        return $now->diff($birthday)->format("%y");
    }

    public function gender()
    {
        return $this->entity->gender == 'M' ? 'Male' : 'Female';
    }

}