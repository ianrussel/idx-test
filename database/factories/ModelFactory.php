<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\State::class, function (Faker\Generator $faker) {
	return [
		'state' => $faker->state,
		'created_by' => $faker->name,
		'updated_by' => $faker->name
	];
});

$factory->define(App\Models\City::class, function (Faker\Generator $faker) {
	return [
		'city' => $faker->city,
		'created_by' => $faker->name,
		'updated_by' => $faker->name
	];
});

$factory->define(App\Models\Student::class, function (Faker\Generator $faker) {
	return [
		'firstname' => $faker->name,
		'lastname' => $faker->lastname,
		'address' => $faker->address
	];
});