<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Model::unguard();

        DB::table('users')->delete();

        $users = array(
        	['name' => 'Christopher Bong Go', 'email' => 'christphergo@yahoo.com', 'password' => Hash::make('secret')],
        	['name' => 'Manny Pinol', 'email' => 'mannypinol@yahoo.com', 'password' => Hash::make('secret')],
        	['name' => 'Jesus Dureza', 'email' => 'jesusdureza@yahoo.com', 'password' => Hash::make('secret')],
        );

        foreach ($$users as $user) 
        {
        	User::create($user);
        }
        Model::reguard();
    }
}
