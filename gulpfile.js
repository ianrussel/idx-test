var gulp = require('gulp');
var elixir = require('laravel-elixir');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minifyCss = require('gulp-minify-css');

//copy  needed files,
//Do gulp 

gulp.task("copyfiles", function() {

    // Copy jQuery, Bootstrap, and FontAwesome
 
  //  gulp.src("resources/assets/less/**")
    //    .pipe(gulp.dest("resources/assets/less/fontawesome"));

    //gulp.src("bower_components/font-awesome/fonts/**")
      //  .pipe(gulp.dest("public/assets/fonts"));

});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
    mix.less('app.less')
        .browserify('app.js');
    var bowerDir = 'bower_components/',
    	jsDir = 'resources/assets/js';
        jQDir = 'public/js';

    mix.copy(bowerDir + 'vue/dist/vue.min.js', jsDir);
    mix.copy(bowerDir + 'vue-resource/dist/vue-resource.min.js', jsDir);
    mix.copy(bowerDir + 'jquery/dist/jquery.min.js', jQDir);

    mix.scripts([
    	'vue.min.js',
    	'vue-resource.min.js', 
    	'all_department.js',
        'project.js'
    ], 'public/js/vendor.js');

   // mix.css([
      //  'all_department.css',
        //'main.css'
    //], 'public/css/projects.css');
});

gulp.task('sass', function() {
    return gulp.src('resources/assets/css/**/*.css')
           .pipe(minifyCss())
           .pipe(concat('projects.css'))
           .pipe(sass())
           .pipe(gulp.dest('public/css'))
})