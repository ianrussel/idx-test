@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h2>Test Javascript Here</h2>
        </div>
        <img src="/images/icon1.png" alt="my images">

        <button class="btn btn-success">Change User</button>
        <h1></h1>
    </div>

@endsection

@push('scripts')
<script>
   var myImage = document.querySelector('img');

    myImage.onclick = function() {
        var mySrc = myImage.getAttribute('scr');
        if (mySrc === '/images/icon2.png') {
            myImage.setAttribute('src', '/images/icon1.png');
        } else {
            myImage.setAttribute('src', '/images/icon2.png');
        }
    }

    var myButton = document.querySelector('button');
    var myHeading = document.querySelector('h1');

   function setUserName() {
       var myName = prompt('Please enter your name.');
       localStorage.setItem('name', myName);
       myHeading.textContent = 'Maxilla is cool, ' + myName;
       if (!localStorage.getItem('name')) {
           setUserName();
   }
    } else {
        var storedName = localStorage.getItem('name');
        myHeading.textContent = 'She is cool' + storedName;
    }
    myButton.onclick = function() {
        setUserName();
    }
</script>
@endpush