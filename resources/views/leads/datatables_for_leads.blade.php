@extends('layouts.app')


@push('scripts')
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
<div class="container">
	<form method="POST" id="search-form" class="form-inline" role="form">
		{!! csrf_field() !!}
        <div class="form-group">
            <label for="first_name">Firstname</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="search firstname">
        </div>
        <div class="form-group">
            <label for="last_name">Lastname</label>
            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="search lastname">
        </div>

        <button type="submit" class="btn btn-primary">Search</button>
    </form>
</div>
<br />
<hr/>
<div class="container">
	<div class="table-responsive">
		<table class="table table-bordered" id="leads-table">
			<thead>
				<tr> 
					<th>Id</th>
					<th>Fullname</th>
					<th>Email</th>
					<th>Birthday</th>
					<th>Edad</th>
					<th>Gender</th>
					<th>Zip</th>
					<th>City</th>
					<th>State</th>
					<th>Address1</th>
					<th>Address2</th>
					<th>Ethnicity</th>
					<th>Phone</th>
					<th>Source Url</th>
					<th>Affiliate Id</th>
					<th>RevenueTracker Id</th>
					<th>Ip</th>
					<th>Mobile?</th>
					<th>Status</th>
					<th>Response</th>
					<th>Created</th>
					<th>Updated</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

@endsection

@push('scripts')
	<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(function() {
			var oTable = $('#leads-table').DataTable({
				dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'>>r>"+
	            "<'row'<'col-xs-12't>>"+
	            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
				processing: true,
				serverSide: true,
				ajax: {
					//url : '{!! route('lead-users.data') !!}',
					url :'/lead-users/data',
					data: function (d) {
						d.first_name = $('input[name = first_name').val();
						d.last_name = $('input[name = last_name').val();
					}
				},
				columns: [
					{ data: 'id',name: 'id'},
					{ data: 'fullname', name: 'fullname', searchable: false, orderable: false},
					{ data: 'email',name: 'email'},
					{ data: 'birthdate', name: 'birthdate'},
					{ data: 'age', name: 'age', searchable: false, orderable: false},
					{ data: 'gender', name: 'gender'},
				    { data: 'zip',name: 'zip'},
					{ data: 'city',name: 'city'},
					{ data: 'state',name: 'state'},
					{ data: 'address1',name: 'address1'},
					{ data: 'address2',name: 'address2'},
					{ data: 'ethnicity',name: 'ethnicity'},
					{ data: 'phone',name: 'phone'},
					{ data: 'source_url',name: 'source_url'},
					{ data: 'affiliate_id',name: 'affiliate_id'},
					{ data: 'revenue_tracker_id',name: 'revenue_tracker_id'},
					{ data: 'ip',name: 'ip'},
					{ data: 'mobile', name: 'mobile', searchable: false, orderable: false},
					{ data: 'status',name: 'status'},
					{ data: 'response',name: 'response'},
					{ data: 'created_at',name: 'created_at'},
					{ data: 'updated_at',name: 'updated_at'}
				]
			});
		
		 	$('#search-form').on('submit', function(e) {
		        oTable.draw();
		        e.preventDefault();
		    });
		});
	</script>
@endpush