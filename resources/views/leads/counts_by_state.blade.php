@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" href="/css/select2.min.css">
@endpush

@section('content')
	<div class="container">
		<form action="{{url('/lead-users/count-per-state')}}" class="form-horizontal" method="GET" id="form-search">
		{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="">Search State</label>
				<select name="state" id="state-autocomplete" class="state-autocomplete form-control">
					<option value=""></option>
					@foreach($states as $i)
						<option value="{{ $i->state}}">{{$i->state}}</option>
					@endforeach
				</select>
			</fieldset>
			<input type="submit" class="btn btn-success" value="Submit">
		</form>
	</div>

	@if(count($allLeadsCount)> 0)
	<div class="container">	
		<div class="page_header"><h2>Total Leads for {{ $state }}&nbsp; State &nbsp;<strong>{{ number_format($allLeadsCount) }}</strong></h2></div>
		<table class="table table-hover">
			<thead>
				<tr>
					<td>Name</td>
					<td>Age</td>
					<td>Email</td>
					<td>City</td>
					<td>State</td>
					<td>Gender</td>
					<td>Is Mobile?</td>
				</tr>
			</thead>
			<tbody>
			    @foreach($allLeadsPerState as $lead)
				<tr>
					<td>{{ $lead->getFullName() }}</td>
					<td>{{ $lead->getAge() }}</td>
					<td>{{ $lead->email }}</td>
					<td>{{ $lead->city }}</td>
					<td>{{ $lead->state }}</td>
					<td>{{ $lead->getGender() }}</td>
					<td>{{ $lead->yesOrNo() }}</td>
					<td></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! $allLeadsPerState->links() !!}
	</div>
	@else
	<div class="container">
		<h2 class="alert alert-danger">	No Query For Leads. Please Search...</h2>
	</div>
	@endif
@endsection

@push('scripts')
    <script src="/js/select2.full.min.js"></script>
    <script>
    	$("#state-autocomplete").select2({ 
                placeholder: "Search by Lead By States", 
            }); 
    </script>
@endpush