@extends('layouts.app')

@push('scripts')
	    <link href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
	    <link rel="stylesheet" href="/css/counts_by_cities.css">
@endpush

@section('content')
	<div class="container">
		<form action="{{ url('lead-users/count-per-city')}}" class="form-horizontal" method="get" id="form-search">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="city_autocomplete">Find City</label>
				<input type="text" class="form-control city-control" id="city_autocomplete" name="city">
			</fieldset> 
			<button type="submit" id="btnNext" class="btn btn-success submit_student">Search</button>
		</form>
	</div>
	@if(count($counts) > 0)
	<div class="container">
		<div class="page_header"><h2>Total Leads for {{ $city }}&nbsp; City &nbsp;<strong>{{ $leadcounts }}</strong></h2></div>
		<table class="table table-bordered table-hover table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Age</th>
					<th>Email</th>
					<th>City</th>
					<th>State</th>
					<th>Gender</th>
					<th>Is Mobile?</th>
				</tr>
			</thead>
			<tbody>
				@foreach($counts as $count)
				<tr>
				    <td id="leads">{{ $count->id }}</td>
					<td>{{ $count->getFullName() }}</td>
					<td>{{ $count->getAge() }}</td>
					<td>{{ $count->email }}</td>
					<td>{{ $count->city }}</td>
					<td>{{ $count->state }}</td>
					<td>{{ $count->getGender() }}</td>
					<td>{{ $count->yesOrNo() }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{ $counts->links() }}
	</div>
	@else
	<div class="container">	
		<h1 class="aalert alert-danger">No Query Data, Try to Search in Search Form!</h1>
	</div>
	@endif
	<div id="divLoading"></div>
@endsection

@push('scripts')
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script>
		$("#city_autocomplete").autocomplete({
			source: '/lead-users/autocomplete',
			minLength: 2,
			select: function(event, ui) {
				$("#divLoading").addClass('show'),
				$("#city_autocomplete").val(ui.item.value);
				$("#form-search").submit();
			}
		});
	</script>
@endpush