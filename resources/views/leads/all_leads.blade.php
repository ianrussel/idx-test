@extends('layouts.app')

@push('scripts')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
<style>
	.control {
		width: 20%;
	}
</style>
@endpush
@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">All Leads</div>
		<div class="table-responsive">
			<table class="table table-bordered" id="all_leads">
				<thead>
					<tr>
						<th>Id</th>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Email</th>
						<th>Birthday</th>
						<th>Gender</th>
						<th>Zip</th>
						<th>City</th>
						<th>State</th>
						<th>Address1</th>
						<th>Address2</th>
						<th>Ethnicity</th>
						<th>Phone</th>
						<th>Source Url</th>
						<th>Affiliate Id</th>
						<th>RevenueTracker Id</th>
						<th>Ip</th>
						<th>Mobile?</th>
						<th>Status</th>
						<th>Response</th>
						<th>Created</th>
						<th>Updated</th>
					</tr>
				</thead>
				<tbody>
					@foreach($all_leads as $lead)
					<tr>
						<td>{{ $lead->id }}</td>
						<td>{{ $lead->first_name }}</td>
						<td>{{ $lead->last_name }}</td>
						<td>{{ $lead->email }}</td>
						<td>{{ $lead->birthdate }}</td>
						<td>{{ $lead->gender }}</td>
						<td>{{ $lead->zip }}</td>
						<td>{{ $lead->city }}</td>
						<td>{{ $lead->state }}</td>
						<td>{{ $lead->address1 }}</td>
						<td>{{ $lead->address2 }}</td>
						<td>{{ $lead->ethnicity }}</td>
						<td>{{ $lead->phone }}</td>
						<td>{{ $lead->source_url }}</td>
						<td>{{ $lead->affiliate_id }}</td>
						<td>{{ $lead->revenue_tracker_id }}</td>
						<td>{{ $lead->ip }}</td>
						<td>{{ $lead->is_mobile }}</td>
						<td>{{ $lead->status }}</td>
						<td>{{ $lead->response }}</td>
						<td>{{ $lead->created_at }}</td>
						<td>{{ $lead->updated_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection

@push('scripts')
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
	<script>
		$(function() {
			$('#all_leads').DataTable({
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			});
		});
	</script>
@endpush