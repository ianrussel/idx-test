@extends('layouts.app')

@push('scripts')
@endpush
@section('content')
	<div class="container">
		<form action="" class="form-horizontal">
			<fieldset class="form-group">
				<label for="gender">Select Leads By Gender</label>
				<select name="gender" id="" class="form-control">
					<option value="M">Male</option>
					<option value="F">Female</option>
					<option value="B">Bay</option>
				</select>
			</fieldset>
			<input type="submit" class="btn btn-success" value="Search">
		</form>
	</div>
	<div class="container">
		<div class="page-header"><h2>Leads By Gender&nbsp;<strong>{{ $sex }} {{ number_format($leads->total()) }}</strong></h2></div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Age</th>
					<th>Email</th>
					<th>City</th>
					<th>State</th>
					<th>Gender</th>
				</tr>
			</thead>
			<tbody>
			@if(count($leads) > 0)
				@foreach($leads as $lead)
				<tr>
					<td>{{ $lead->getFullName() }}</td>
					<td>{{ $lead->getAge() }}</td>
					<td>{{ $lead->email }}</td>
					<td>{{ $lead->city }}</td>
					<td>{{ $lead->state }}</td>
					<td>{{ $lead->getGender() }}</td>
				</tr>
				@endforeach
				@else
			<h2>Walay Bay nga Leads</h2>
				@endif
			</tbody>
		</table>
		{!! $leads->links() !!}
	</div>
@endsection

@push('scripts')

@endpush