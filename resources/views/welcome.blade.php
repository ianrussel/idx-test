@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <h3>Users</h3>
                    <p>You may try this users or you may register, its free :)</p>
                    <ul>
                        <li>(role = Encoder) email: johnny@yahoo.com |password = 1234567</li>
                        <li>(role = Admin) email: juan@yahoo.com |password = juan@yahoo.com</li>
                    </ul>
                    <p>Any users can register and login. By default, any un-authenticated users has no access to some routes or view, while some authenticated users has some access to all routes, or views or whatever it should be called. A user with Administrator role can define a role/s to a user, example an Encoder, Beautician, Accountant, Manager, etcetera. Every roles has its corresponding permission, for example, An Encoder role can have permissions attached('can add student, can delete, can view').</p><br/>
                    <h3>Add Student</h3>
                    <p>A student has one city, and city has many students. To add new student, first create or add new city, city is related to States, you can add States or city, just ask your admin</p><br/>
                    <h3>Authorization</h3>
                    <p>Currently , two Roles were enabled, 'Admin' and 'Encoder'. You can add as many roles and attach as many permissions to it. You can access it in Admin area.</p><br />

                    <h3>Reports</h3>
                    <p>I am relatively new to web development, and Symfony is my first PHP Framework, am currently learning this Amazing popular PHP Framework called Laravel by Taylor Otwel. Expected, You may find bugs or un-Laravel way of writing codes mostly in Controllers, but since I am eager to learn this amazing Framework, I am practicing frequently writing code in Laravel, read its docs more frequent than dating my wife. As per requirement, you can export student records in excel, pdf, csv formats or even print it directly from the browser, again some specific routes requires permissions attached to role</p>

                    <h3>Database and Migration</h3>
                    <p>There some situation that running migration in Laravel will throw sql related errors, instead of scratching your head to fix it, I opted to include sql files for this application</p><br/>
                    <p>I prefer Linux here, the sql file is in sql_file folder and its filename is idx_test.sql</p><br>
                    <p>cd /var/www/topath</p><br />
                    <p>mysql -u root -p</p><br />
                    <p>create database idx</p><br />
                    <p>use idx</p><br />
                    <p>source /var/www/yourproject/sql_file/idx_test.sql</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
