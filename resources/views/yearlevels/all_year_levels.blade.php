@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">All Year Level</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>yearlevel</th>
					<th>Created By</th>
					<th>Updated By</th>
					<th>Created At</th>
					<th>Updated At</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allYearlevels as $level)
				<tr>
					<td>{{ $level->id }}</td>
					<td>{{ $level->yearlevel }}</td>
					<td>{{ $level->created_by }}</td>
					<td>{{ $level->updated_by }}</td>
					<td>{{ $level->created_at}}</td>
					<td>{{ $level->updated_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! $allYearlevels->links() !!}
	</div>
	<div class="container">
		<button class="btn btn-primary" data-toggle="collapse" href="#add_year_level" id="add_year_level_button">Add New Year Level</button>
	</div><br />
	<div class="container collapse" id="add_year_level">
		<form action="{{url('/idx-test/year-level/add-new-year-levels')}}" method="POST" class="form">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="section">Year Level</label>
				<input type="text" name="yearlevel" id="yearlevel" class="form-control">
			</fieldset>
			<button class="btn btn-default">Submit</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush