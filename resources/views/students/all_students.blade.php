@extends('layouts.app')

@section('content')
	@if(Auth::user()->isActive == 0)
	<div class="container">
		<h1>Sorry {{Auth::user()->name}}, You are dead, your account has been suspended!</h1>
		<h2>Kindly ask your administrator to bring you back in life!</h2>
	</div>
	@else
	<div class="container">
		<button class="btn btn-default" id="add_student_button">Add Student</button>
	</div>
	<br/>
	<div class="container" id="all_student">
		@include('common.success')
		@include('common.errors')
		<div class="page_header">All Students</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Student Id</th>
					<th>Name</th>
					<th>Address</th>
					<th>Section</th>
					<th>Year Level</th>
					<th>Age</th>
					<th>Created By</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allStudents as $student)
				<tr>
					<td>{{ $student->id }}</td>
					<td>{{ $student->getStudentId() }}</td>
					<td>{{ $student->getFullname() }}</td>
					<td>{{ $student->getAddress() }}</td>
					<td>{{ $student->section->section }}</td>
					<td>{{ $student->yearlevel->yearlevel }}</td>
					<td>{{ $student->getAge() }}</td>
					<td>{{ $student->created_by }}</td>
					<td><a href="{{url('/idx-test/show-student/'. $student->id )}}">Show</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! $allStudents->links() !!}
	</div>
	<div class="container">
		<div id="add_student_div" style="display:none">
			<h2>Add New Student</h2>
			<form action="{{ url('idx-test/add-new-students')}}" class="add_student_form" method="POST" id="add_student_form">
				<input type="hidden" name="_method" value="POST">
				{!! csrf_field() !!}
				<fieldset class="form-group">
					<label for="firstname" class="">Firstname</label>
					<input type="text" id="firstname" name="firstname" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="lastname" class="">Lastname</label>
					<input type="text" id="lastname" name="lastname" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="bday" class="">Birthday</label>
					<input type="text" id="bday" name="bday" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="address" class="">Address</label>
					<input type="text" id="address" name="address" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="city" class="">City</label>
				    <select name="city_id" id="city_id" class="form-control">
				        @foreach ($allCities as $city)
				            <option selected value="{{ $city->id }}">{{ $city->city }}</option>
				          @endforeach
				    </select>
				    <small class="text-muted">You can also add city(s) in admin area</small>
				</fieldset>
				<fieldset class="form-group">
					<label for="zip" class="">Zip Code</label>
					<input type="text" id="zip" name="zip" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="phone" class="">Phone</label>
					<input type="text" id="phone" name="phone" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="mobile" class="">Mobile</label>
					<input type="text" id="mobile" name="mobile" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="email" class="">Email</label>
					<input type="email" id="email" name="email" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="section" class="">Section</label>
				    <select name="section_id" id="section_id" class="form-control">
				        @foreach ($allSections as $section)
				            <option selected value="{{ $section->id }}">{{ $section->section }}</option>
				        @endforeach
				        </select>
				    <small class="text-muted">You can also add sections in admin area</small>
				</fieldset>
				<fieldset class="form-group">
					<label for="yearlevel" class="">Year Level</label>
				    <select name="yearlevel_id" id="yearlevel_id" class="form-control">
				        @foreach ($allYearlevels as $yearlevel)
				           	<option selected value="{{ $yearlevel->id }}">{{ $yearlevel->yearlevel }}</option>
				        @endforeach
				    </select>
				    <small class="text-muted">You can also add year levels in admin area</small>
				</fieldset>
				<button type="submit" id="btnNext" class="btn btn-default submit_student">Next</button>
			</form>
		</div>
	</div>
	@endif
@endsection

@push('scripts')
	<script>
		$("#add_student_button").click(function() {
			$("#add_student_div").slideToggle("slow","easeOutBounce", function(){
				$("#all_student").hide();
				$("#add_student_button").hide();
			});
		});
	</script>

	<!--script>
		var q = 1,
	    qMax = 0;

		$(function () {
		    qMax = $('#add_student_form fieldset.form-group').length;
		    $('#add_student_form fieldset.form-group').hide();
		    $('#add_student_form fieldset.form-group:nth-child(1)').show();
		    $('#btnNext').on('click', function (event) {
		        event.preventDefault();
		        handleClick();
		    });
		});

		function handleClick() {
		    if (q < qMax) {
		        $('#add_student_form fieldset.form-group:nth-child(' + q + ')').hide();
		        $('#add_student_form fieldset.form-group:nth-child(' + (q + 1) + ')').show();
		        if (q == (qMax - 1)) {
		            $('#btnNext').html('Submit');
		        }
		        q++;
		    } else {
		        window.location +'/idx-test/add-new-students'; // Add code to submit your form
		    }
		}
	</script-->
	<script src="/js/date.js"></script>
	<script src="/js/alert-success.js"></script>
@endpush