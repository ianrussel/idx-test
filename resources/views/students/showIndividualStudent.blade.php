@extends('layouts.app')

@push('scripts')
	<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
@endpush
@section('content')
	<div class="container" id="student-info">
		@include('common.success')
		@include('common.errors')
		<div class="page_header"><h1>{{ $student->getFullname() }}</h1></div>
		<table class="table table-bordered table-condensed" id="student-info-table">
			<thead>
				<tr>
					<th>Student Id</th>
					<th>Name</th>
					<th>Student Address</th>
					<th>Student Age</th>
					<th>Student Section</td>
					<th>Student Year Level</td>
					<th>Zip</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Mobile</th>
					<th>Created By</th>
					<th>Updated By</th>
					<th>Created At</th>
					<th>Updated At</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $student->getStudentId() }}</td>
					<td>{{ $student->getFullname() }}</td>
					<td>{{ $student->getAddress() }}</td>
					<td>{{ $student->age }}</td>
					<td>{{ $student->section->section }}</td>
				    <td>{{ $student->yearlevel->yearlevel }}</td>
					<td>{{ $student->zip }}</td>
					<td>{{ $student->email }}</td>
				    <td>{{ $student->phone }}</td>
					<td>{{ $student->mobile }}</td>
					<td>{{ $student->created_by }}</td>
					<td>{{ $student->updated_by }}</td>
					<td>{{ $student->created_at }}</td>
					<td>{{ $student->updated_at }}</td>
				</tr>
			</tbody>
		
		</table>
		<div class="form form-group" id="edit-delete-button">
			<div class="sm-offset-3 col-sm-6">
				<button class="btn btn-default" id="edit-button">Edit</button>|or|
				<form action="{{ url('/idx-test/delete/delete-this-student/'. $student->id) }}" method="POST">
				{!! csrf_field() !!}
				{!! method_field('DELETE') !!}
				<button class="btn btn-warning" id="delete-button" onclick="confirm('are you sure?, this cannot be undone!')">Delete</button>
				</form>
			</div>
		</div>
	</div>
	<div class="container" id="edit-student-info" style="display:none">
		<form action="{{ url('idx-test/update/update-this-student/'. $student->id)}}" class="form-horizontal" method="post">
			<input type="hidden" name="_method" value="PATCH">
			<input type="hidden" name="firstname" value="{{ $student->firstname }}">
			<input type="hidden" name="lastname" value="{{ $student->lastname }}">
			{{ csrf_field() }}
			<div class="page_header"><h1>{{ $student->getFullname() }}</h1></div>
			<table class="table table-bordered" id="edit-student-info">
				<tr>
					<td>Student Id</td>
					<td><input type="text" value="{{ $student->getStudentId() }}" class="form-control" readonly></td>
				</tr>
				<tr>
					<td>Student Address{{ $student->getAddress() }}</td>
					<td><input type="text" name="address" value="{{ $student->address }}" class="form-control"></td>
				</tr>
				<tr>
					<td>City</td>
					<td>
				        <select name="city_id" id="city_id" class="form-control">
				          @foreach ($allCities as $city)
				            <option selected value="{{ $city->id }}">{{ $city->city }}</option>
				          @endforeach
				        </select>
					</td>
				</tr>
				<tr>
					<td>Student Birthdate( current age {{ $student->getAge() }})</td>
					<td><input type="text" id="bday" name="bday" value="{{$student->bday}}" class="form-control"></td>
				</tr>
				<tr>
					<td>Student Section ( current section {{ $student->section->section }})</td>
					<td>
				        <select name="section_id" id="section_id" class="form-control">
				          @foreach ($allSections as $section)
				            <option selected value="{{ $section->id }}">{{ $section->section }}</option>
				          @endforeach
				        </select>
					</td>
				</tr>
				<tr>
					<td>Student Year Level (current year level {{ $student->yearlevel->yearlevel }})</td>
					<td>
				        <select name="yearlevel_id" id="yearlevel_id" class="form-control">
				          @foreach ($allYearlevels as $yearlevel)
				            <option selected value="{{ $yearlevel->id }}">{{ $yearlevel->yearlevel }}</option>
				          @endforeach
				        </select>
					</td>
				</tr>
				<tr>
					<td>Zip</td>
					<td><input type="text" name="zip" value="{{ $student->zip }}" class="form-control"></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="email" name="email" value="{{ $student->email }}" class="form-control"></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><input type="text" name="phone" value="{{ $student->phone }}" class="form-control"></td>
				</tr>
				<tr>
					<td>Mobile</td>
					<td><input type="text" name="mobile" value="{{ $student->mobile }}" class="form-control"></td>
				</tr>
			</table>
			<div class="form form-group">
				<button class="btn btn-default" name="submit">Edit</button>
			</div>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
	<script src="/js/alert-success.js"></script>
	<script src="/js/date.js"></script>
	<script>
		$(document).ready(function() {
			$('#student-info-table').DataTable({
				dom: 'Bfrtip',
				bFilter: false,
				bPaginate: false,
				bSort: false,
				buttons: [ {
					extend: 'excelHtml5',
					customize: function(xlsx) {
						var sheet = xlsx.xl.worksheets['sheet1.xml'];

						//jQuery selector to add border
						$('row c[r*="10"]', sheet).attr('s', '25');
					}
				}]
			});
		});
	</script>
	<script>
		$('#edit-button').click( function() {
			$("#edit-student-info").show();
			$("#student-info").hide();
			$("#edit-delete-button").hide();
		});
	</script>
@endpush