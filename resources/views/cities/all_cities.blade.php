@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">All Cities</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>City</th>
					<th>State</th>
					<th>Created By</th>
					<th>Updated By</th>
					<th>Created At</th>
					<th>Updated At</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allCities as $city)
				<tr>
					<td>{{ $city->id }}</td>
					<td>{{ $city->city }}</td>
					<td>{{ $city->state->state }}</td>
					<td>{{ $city->created_by }}</td>
					<td>{{ $city->updated_by }}</td>
					<td>{{ $city->created_at }}</td>
					<td>{{ $city->updated_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		{!! $allCities->links() !!}
	</div>
	<div class="container">
		<button class="btn btn-primary" data-toggle="collapse" href="#add_city" id="add_city_button">Add New City</button>
	</div>
	<div class="container collapse" id="add_city">
		<div class="card card-block">
			<form action="{{url('/idx-test/add-new-city')}}" method="POST" class="form">
				{!! csrf_field() !!}
				<fieldset class="form-group">
					<label for="city">City</label>
					<input type="text" name="city" id="city" class="form-control">
				</fieldset>
				<fieldset class="form-group">
					<label for="state_id">State</label>
				    <select name="state_id" id="state_id" class="form-control">
				        @foreach ($states as $state)
				           	<option selected value="{{ $state->id }}">{{ $state->state }}</option>
				        @endforeach
				    </select>
				    <small class="text-muted">You can also add year levels in admin area</small>
				</fieldset>
				<button class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
@endsection