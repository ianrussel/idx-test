@extends('layouts.app')

@section('content')

<div class="container" id="CityController">
	<div class="container">
		<div class="alert alert-danger" v-if="isValid">
			<ul>
				<li v-show="!validation.city">City field is required</li>
				<li v-show="!validation.state">State field is required</li>
			</ul>
		</div>
	</div>
	<div class="container">
		<form action="#" method="POST" @submit.prevent="AddNewCity" class="form-horizontal" role="form">
			<fieldset class="form-group">
				<label for="city">City:</label>
				<input v-model="newCity.city" type="text" name="city" id="city" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<label for="state_id">State:</label>
				<input v-model="newCity.state_id" type="text" name="state_id" id="state_id" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<button :disabled="!isValid" class="btn btn-default" type="submit" v-if="!edit">Add New City</button>
				<button :disabled="!isValid" class="btn btn-default" type="submit" v-if="edit" @click="editCity(newCity.id)">Edit</button>
			</fieldset>
		</form>
		<div class="alert alert-success" transition="sucess" v-if="success">Add New City Successful!</div>
	</div>
	<hr>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>City</th>
				<th>State</th>
				<th>Created By</th>
				<th>Updated By</th>
				<th>Created At</th>
				<th>Updated At</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr v-for="city in cities">
				<td>@{{ city.city }}</td>
				<td>@{{ city.state.state }}</td>
				<td>@{{ city.created_by }}</td>
				<td>@{{ city.updated_by }}</td>
				<td>@{{ city.created_at }}</td>
				<td>@{{ city.updated_at }}</td>
				<td>
					<button class="btn btn-default btn-sm" @click="showCity(city.id)">Edit</button>
					<button class="btn btn-danger btn-sm" @click="removeCity(city.id)">Remove</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>

@endsection

@push('scripts')
<script>
	var vue = new Vue({
	http: {
		root: '/root',
		headers: {
			'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
		}
	},

	el: '#CityController',

	data: {
		newCity: {
			id: '',
			city: '',
			state_id: ''
		},
		success: false,

		edit: false
	},

	methods: {
		allCities: function() {
			this.$http.get('/idx-test/cities', function(data) {
				this.$set('cities', data)
			})
		},
		removeCity: function(id) {
			var ConfirmBox = confirm("Are you sure to delete?, this cannot be undone!")
			if(ConfirmBox) this.$http.delete('/idx-test/city-delete/' + id)

			this.allCities()
		    this.refresh
		},

		editCity: function(id) {
			var city = this.newCity

			this.newCity = {id: '', city: '', state: ''}
			this.$http.patch('/idx-test/cities/' + id, city, function(data) {
				console.log(data)
			})
			this.allCities()
			this.edit = false
		},

		showCity: function(id) {
			this.edit = true
			this.$http.get('/idx-test/edit/cities/' + id, function(data) {
				this.newCity.id = data.id
				this.newCity.city = data.city
				this.newCity.state = data.state
			})
		},

		AddNewCity: function() {
			// user input
			var city = this.newCity;

			//clear form
			
			this.newCity = {city: '', state: ''}

			//post data
			this.$http.post('/idx-test/add-new-city', city)

			//show success message
			self = this
			this.success = true

			setTimeout(function() {
				self.success = false
			}, 5000)

			//reload 
			this.allCities()
		}
	},

	computed: {
		validation: function() {
			return {
				city: !!this.newCity.city.trim(),
				state: !!this.newCity.state.trim()
			}
		},

		isValid: function() {
			var validation = this.validation
			return Object.keys(validation).every(function(key) {
				return validation[key]
			})
		}
	},

	ready: function() {
		this.allCities()
	}
});
</script>
	
@endpush