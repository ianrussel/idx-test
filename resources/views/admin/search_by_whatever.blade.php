@extends('layouts.app')

@push('scripts')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
<style>
	.control {
		width: 20%;
	}
</style>
@endpush
@section('content')
<div class="container">
	<form action="{{url('/idx-test/level/section/city/zip/whatever')}}" class="form" method="get">
	{!! csrf_field() !!}
		<div class="page_header"><h2>Search By Whatever</h2></div>
		<fieldset class="form-group">
			<label for="section" class="">Sections</label>
		    <select name="section_id" id="section_id" class="form-control control">
		        @foreach ($Allsections as $section)
		            <option selected value="{{ $section->id }}">{{ $section->section }}</option>
		        @endforeach
		    </select>
		</fieldset>
		<fieldset class="form-group">
			<label for="city" class="">Year Level</label>
			    <select name="yearlevel_id" id="yearlevel_id" class="form-control control">
			        @foreach ($Allyearlevels as $yearlevel)
			            <option selected value="{{ $yearlevel->id }}">{{ $yearlevel->yearlevel }}</option>
			        @endforeach
			    </select>
		</fieldset>	
		<fieldset class="form-group">
			<label for="city" class="">City</label>
		    <select name="city_id" id="city_id" class="form-control control">
		        @foreach ($Allcities as $city)
		            <option selected value="{{ $city->id }}">{{ $city->city }}</option>
		        @endforeach
		    </select>
		</fieldset>
		<button class="btn btn-primary">Submit</button>
	</form>
</div>
<div class="container">
<hr/>
</div>
<div class="container">
	<div class="page_header">Students</div>
	<table class="table table-bordered" id="students-table-whatever">
		<thead>
			<tr>
				<th>Id</th>
				<th>Student Id</th>
				<th>Name</th>
				<th>Address</th>
				<th>City</th>
				<th>Section</th>
				<th>Level</th>
				<th>Age</th>
			</tr>
		</thead>
		<tbody>
			@foreach($Allstudents as $student)
			<tr>
				<td>{{ $student->id }}</td>
				<td>{{ $student->getStudentId() }}</td>
				<td>{{ $student->getFullname() }}</td>
				<td>{{ $student->address }}</td>
				<td>{{ $student->city->city }}</td>
				<td>{{ $student->section->section }}</td>
				<td>{{ $student->yearlevel->yearlevel }}</td>
				<td>{{ $student->age }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@push('scripts')
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
	<script>
		$(function() {
			$('#students-table-whatever').DataTable({
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			});
		});
	</script>
	<!--ANGULAR JS -->
	<script>
		
	</script>
@endpush