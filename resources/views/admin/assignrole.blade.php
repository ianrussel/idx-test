@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.success')
		<h2>Assign Roles</h2>
		<form action="{{ url('idx-test/assign-roles')}}" method="POST">
			<input type="hidden" name="_method" value="POST">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="user" class="">User</label>
			    <select name="email" id="" class="form-control">
			        @foreach ($allUsers as $user)
			            <option selected value="{{ $user->email }}">{{ $user->name }} {{ $user->email }}</option>
			         @endforeach
			    </select>
			    <small class="text-muted">You can also add users in admin area</small>
			</fieldset>
			<fieldset class="form-group">
				<label for="section" class="">Roles</label>
			    <select name="role" id="" class="form-control">
			        @foreach ($allRoles as $role)
			            <option selected value="{{ $role->name }}">{{ $role->name }}</option>
			        @endforeach
			        </select>
			    <small class="text-muted">You can also add roles in admin area</small>
			</fieldset>
			<button type="submit" class="btn btn-success">Submit</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush
