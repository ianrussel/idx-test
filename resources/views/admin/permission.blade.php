@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">Permission</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Display</th>
					<th>Description</th>
					<th>Created By</th>
					<th>Updated By</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allPermissions as $permission)
				<tr>
					<td>{{ $permission->id }}</td>
					<td>{{ $permission->name }}</td>
					<td>{{ $permission->display_name }}</td>
					<td>{{ $permission->description }}</td>
					<td>{{ $permission->created_by }}</td>
					<td>{{ $permission->updated_by }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="container">
		<div class="page_header"><h2>Add New Permissions</h2></div>
	</div>
	<div class="container">
		<form action="{{ url('/idx-test/permissions')}}" method="POST">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="role">Permission</label>
				<input type="text" name="name" id="name" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<label for="role">Display Name</label>
				<input type="text" name="display_name" id="display_name" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<label for="role">Description</label>
				<input type="text" name="description" id="description" class="form-control">
			</fieldset>
			<button class="btn btn-success">Submit</button>
		</form>
	</div>
@endsection
@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush