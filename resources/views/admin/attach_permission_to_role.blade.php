@extends('layouts.app')

@section('content')

	<div class="container">
		@include('common.success')
		@include('common.errors')
		<h2>Attach Permission To Roles</h2>
		<form action="{{ url('/idx-test/attach-permissions')}}" method="POST">
			<input type="hidden" name="_method" value="POST">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="section" class="">Roles</label>
			    <select name="role" id="" class="form-control">
			        @foreach ($allRoles as $role)
			            <option selected value="{{ $role->name }}">{{ $role->name }}</option>
			        @endforeach
			        </select>
			    <small class="text-muted">You can also add roles in admin area</small>
			</fieldset>
			<fieldset class="form-group">
				<label for="permission" class="">Permissions</label>
			    <select name="permission" id="" class="form-control">
			        @foreach ($allPermissions as $permission)
			            <option selected value="{{ $permission->name }}">{{ $permission->name }}</option>
			         @endforeach
			    </select>
			    <small class="text-muted">You can also add permissions in admin area</small>
			</fieldset>
			<button type="submit" class="btn btn-success">Submit</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush