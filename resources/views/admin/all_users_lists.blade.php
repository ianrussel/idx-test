@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">All Users</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
					<th>IsActive</th>
					<th>Created By</th>
					<th>Updated By</th>
					<th>Disable</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allUsers as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>@if($user->isActive == 1)
						Yes
						@else
						No
						@endif
					</td>
					<td>Himself</td>
					<td>{{ $user->updated_by }}</td>
					<td><a href="{{url('/idx-test/show-user/'. $user->id )}}">Show</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection