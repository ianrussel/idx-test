@extends('layouts.app')

@section('content')
	<div class="container"><h1>User</h1></div>
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<form action="{{ url('idx-test/edit-user/'. $user->id)}}" class="form-horizontal" method="post">
			<input type="hidden" name="_method" value="PATCH">
			{{ csrf_field() }}
			<div class="page_header"><h1>{{ $user->name }} {{ $user->email }}</h1></div>
			<table class="table table-bordered" id="edit-student-info">
				<tr>
					<td>User Id</td>
					<td><input type="text" value="{{ $user->id }}" class="form-control" readonly></td>
				</tr>
				<tr>
					<td>Status @if($user->isActive==0) Deactivated Account @else  Active @endif</td>
					<td>
						<select name="isActive" class="form-control">
				            <option value="0">Deactivate</option>
				            <option value="1">Activate</option>
				    	</select>
					</td>
					<small class="text-muted">Activate or Deactivate user</small>
				</tr>
			</table>
			<div class="form form-group">
				<button class="btn btn-default" name="submit">Edit</button>
			</div>
		</form>
	</div>
@endsection
@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush

