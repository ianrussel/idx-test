@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="page_header"><h2>Add Roles</h2></div>
	</div>
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<form action="{{ url('/idx-test/roles')}}" method="POST">
			{!! csrf_field() !!}
			<fieldset class="form-group">
				<label for="role">Role</label>
				<input type="text" name="name" id="name" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<label for="role">Display Name</label>
				<input type="text" name="display_name" id="display_name" class="form-control">
			</fieldset>
			<fieldset class="form-group">
				<label for="role">Description</label>
				<input type="text" name="description" id="description" class="form-control">
			</fieldset>
			<button class="btn btn-success">Submit</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush