@extends('layouts.app')
@push('scripts')
	<!--link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"-->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
@endpush
@section('content')

<div class="container">
  	<h2>Student Reports</h2>
  	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#home">Group by level and section</a></li>
	    <li><a data-toggle="tab" href="#menu1">By level, section, city, zip</a></li>
	    <li><a data-toggle="tab" href="#menu2">By Age</a></li>
	    <li><a data-toggle="tab" href="#menu3">Search by firstname</a></li>
  	</ul>

  	<div class="tab-content">
	    <div id="home" class="tab-pane fade in active">
	      	<h4>Export</h4>
		    <table class="table table-bordered" id="students-table">
		    	<thead>
		    		<tr>
		    			<th>Student Id</th>
		    			<th>Firstname</th>
		    			<th>Lastname</th>
		    			<th>Section</th>
		    			<th>Level</th>
		    		</tr>
		    	</thead>
		   	</table>
	    </div>
	    <!--end -->
	    <div id="menu1" class="tab-pane fade">
	      	<h3>Menu 1</h3>
	      	<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	    </div>
	    <div id="menu2" class="tab-pane fade">
	      	<h3>Menu 2</h3>
	      	<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
	    </div>
	    <div id="menu3" class="tab-pane fade">
	      	<h3>Menu 3</h3>
	      	<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	    </div>
  	</div>
</div>
@endsection

@push('scripts')
	<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script-->
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script-->
	<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
	<script>
		/*$(document).ready(function() {
			$('#students-table').DataTable({
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			});
		});*/
		$(function() {
			$('#students-table').DataTable({
				processing: true,
				serverSide: true,
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				],
				ajax: '{!! route('datatables.data') !!}',
				columns: [
					{ data: 'id', name: 'students.id' },
					{ data: 'firstname', name: 'students.firstname'},
					{ data: 'lastname', name: 'students.lastname'},
					{ data: 'section.section', name: 'section.section'},
					{ data: 'yearlevel.yearlevel', name: 'yearlevel.yearlevel'}

				],
				order: [['1', 'desc']]
			});
		});
	</script>
@endpush