<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>test</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>
<style>
	body {
		height:600px;
		width:100%;
	}
</style>
</head>
<body>
	<p id="test" data-id="{{$data}}"></p>
	<h1>Test</h1>
<canvas id="myChart" width="400" height="400"></canvas>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
var a = $('#test').attr('data-id');
var b = JSON.parse(a);
var ctx = document.getElementById("myChart").getContext("2d");
var cities = [];
var counts = [];
b.reduce(function(data, obj){ cities.push(obj.city)});
b.reduce(function(data, obj){ counts.push(obj.count)});
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
    	labels: cities,
        datasets: [{
            label: '# of Population',
            data: counts,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

</script>
</body>
</html>