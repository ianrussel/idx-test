<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="token" id="token" value="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Idx Test</title>

    <!-- Fonts -->
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700"-->

    <!-- Styles -->
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"-->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/main.css">
<<<<<<< HEAD
    @stack('styles')
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    IDX Test
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
<<<<<<< HEAD
                    @if(Auth::check())
                    @if(Auth::user()->name)
                    <li><a href="{{ url('/idx-test/all-students')}}">Students</a></li>
                    <li><a href="{{ url('/idx-test/lead_users') }}">Lead Users</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Lead Users Datatables<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/lead-users')}}">Datatables</a></li>  
                            <li><a href="{{ url('/lead-users/count-per-city') }}">Leads Per City(Jquery Autocomplete)</a></li>  
                            <li><a href="{{ url('/lead-users/count-per-state')}}">Leads Per State(Select2)</a></li>
                            <li><a href="{{ url('/lead-users/count-lead-by-gender') }}">Leads Per Gender</a></li>
                            <li><a href="{{ url('/lead-users/test-javascript') }}">Test Javascript</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Lead Users Charts<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/lead-users/lead-users/lava/lead-users-chart')}}">Lead User Chart</a></li>  
                        </ul>
                    </li>
                    @endif  

                    @endif          
=======
                    @if(Auth::user())
                    <li><a href="{{ url('/idx-test/all-students')}}">Students</a></li>
                    @endif            
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
<<<<<<< HEAD
                    @if(Auth::check())
                    @if (!Auth::user()->name)
=======
                    @if (Auth::guest())
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Admin Crud<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                            
                            @if(Auth::user()->hasRole('Admin'))
                                <li><a href="{{ url('/idx-test/states') }}">States</a></li>  
                                <li><a href="{{ url('/idx-test/cities') }}">Cities</a></li>  
                                <li><a href="{{ url('/idx-test/student-sections') }}">Sections</a></li>
                                <li><a href="{{ url('/idx-test/year-levels') }}">Year Level</a></li>
                            @else
                                <li>Only User with Admin Roles Can Access this facility</li>
                            @endif
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Users<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
<<<<<<< HEAD
                            @if(Auth::user()->hasRole('Admin')) <li><a href="{{ url('/idx-test/api/users') }}">Users</a></li> <li><a href="{{ url('/idx-test/allroles') }}">Roles</a></li>
=======
                            @if(Auth::user()->hasRole('Admin'))
                                <li><a href="{{ url('/idx-test/api/users') }}">Users</a></li>  
                                <li><a href="{{ url('/idx-test/allroles') }}">Roles</a></li>  
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
                                <li><a href="{{ url('/idx-test/allpermissions') }}">Permissions</a></li>
                                <li><a href="{{ url('/idx-test/show-assign-roles-form') }}">Assign Roles</a></li>
                                <li><a href="{{ url('/idx-test/display_attach_permission_to_role')}}">Assign Permission</a></li>
                            @else
                                <li>You cant access this Facility, Ask admin</li>
                            @endif
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Admin Reports<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->hasRole('Admin'))
                                <li><a href="{{ url('datatables') }}">Report 1</a></li>
                                <li><a href="{{ url('/idx-test/age/search-by-age')}}">By Age</a></li>
                                <li><a href="{{ url('/idx-test/level/section/city/zip/whatever')}}">By Level Section Whatever</a></li>
                            @else
                                <li>You Cannot Access this facility, ask your admin</li>
                            @endif
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
<<<<<<< HEAD
                    @endif
=======
>>>>>>> 65938a7404ccaae6a74bff56bbecabb555ebf63f
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper">
    @yield('content')
    </div>
    <!-- Footer -->
    <footer>
        <div class="wrapper">
            <hr />
            <p class="text-center">Copyright &copy; IDX Test {{ date("Y") }}. All rights reserved.</p>
        </div>
    </footer>

    <!-- JavaScripts -->
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script-->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <!--script src="/js/app.js"></script-->
    <script src="/js/jquery.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/vue.js"></script>
    <script src="/js/angular.js"></script>

    @stack('scripts')
</body>
</html>
