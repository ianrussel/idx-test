@extends('layouts.app')

@section('content')
	<div class="container">
		@include('common.errors')
		@include('common.success')
		<div class="page_header">All States</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>States</th>
					<th>Created By</th>
					<th>Updated By</th>
					<th>Created At</th>
					<th>Updated At</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allStates as $state)
				<tr>
					<td>{{ $state->state }}</td>
					<td>{{ $state->created_by }}</td>
					<td>{{ $state->updated_by }}</td>
					<td>{{ $state->created_at }}</td>
					<td>{{ $state->updated_at}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! $allStates->links() !!}
	</div>
	<div class="container">
		<button class="btn btn-primary" data-toggle="collapse" href="#add_state" id="add_city_button">Add New State</button>
	</div>
	<div class="container collapse" id="add_state">
		<div class="card card-block">
			<form action="{{url('/idx-test/states/add-new-state')}}" method="POST" class="form">
				{!! csrf_field() !!}
				<fieldset class="form-group">
					<label for="state">State</label>
					<input type="text" name="state" id="state" class="form-control">
				</fieldset>
				<button class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
@endsection

@push('scripts')
	<script src="/js/alert-success.js"></script>
@endpush