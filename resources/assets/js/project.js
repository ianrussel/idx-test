var vue = new Vue({
	http: {
		root: '/root',
		headers: {
			'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
		}
	},

	el: '#ProjectController',

	data: {
		newProject: {
			id: '',
			project: '',
			description: ''
		},
		success: false,

		edit: false
	},

	methods: {
		allProjects: function() {
			this.$http.get('/api/projects', function(data) {
				this.$set('projects', data)
			})
		},
		removeProject: function(id) {
			var ConfirmBox = confirm("Are you sure to delete?, this cannot be undone!")
			if(ConfirmBox) this.$http.delete('/api/projects/' + id)

			this.allProjects()
		    this.refresh
		},

		editProject: function(id) {
			var project = this.newProject

			this.newProject = {id: '', project: '', description: ''}
			this.$http.patch('/api/projects/' + id, project, function(data) {
				console.log(data)
			})
			this.allProjects()
			this.edit = false
		},

		showProject: function(id) {
			this.edit = true
			this.$http.get('/api/projects/' + id, function(data) {
				this.newProject.id = data.id
				this.newProject.project = data.project
				this.newProject.description = data.description
			})
		},

		AddNewProject: function() {
			// user input
			var project = this.newProject;

			//clear form
			
			this.newProject = {project: '', description: ''}

			//post data
			this.$http.post('/api/projects', project)

			//show success message
			self = this
			this.success = true

			setTimeout(function() {
				self.success = false
			}, 5000)

			//reload 
			this.allProjects()
		}
	},

	computed: {
		validation: function() {
			return {
				project: !!this.newProject.project.trim(),
				description: !!this.newProject.description.trim()
			}
		},

		isValid: function() {
			var validation = this.validation
			return Object.keys(validation).every(function(key) {
				return validation[key]
			})
		}
	},

	ready: function() {
		this.allProjects()
	}
});