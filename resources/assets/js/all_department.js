$(document).ready(function(){

    var url = 'api/departments';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    //display modal form for department editing
    $('.open-modal').click(function(){
        var department_id = $(this).val();

        $.get(url + '/' + department_id, function (data) {
            //success data
            console.log(data);
            $('#department_id').val(data.id);
            $('#department').val(data.department);
            $('#btn-save').val("update");

            $('#myModal').modal('show');
        }) 
    });

    //display modal form for creating new department
    $('#btn-add').click(function(){
        $('#btn-save').val("add");
        $('#frmDepartments').trigger("reset");
        $('#myModal').modal('show');
    });

    //delete department and remove it from list
    $('.delete-department').click(function(){
        var department_id = $(this).val();

        $.ajax({

            type: "DELETE",
            url: url + '/' + department_id,
            success: function (data) {
                console.log(data);

                $("#department" + department_id).remove();
            },
            error: function (data) {
            console.log('Error:', data);
            }
        });
    });

    //create new department / update existing department
    $("#btn-save").click(function (e) {
       $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

        e.preventDefault(); 

        var formData = {
            department: $('#department').val(),
            //description: $('#description').val(),
        }

        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();

        var type = "POST"; //for creating new resource
        var department_id = $('#department_id').val();;
        var my_url = url;

        if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/' + department_id;
        }

        console.log(formData);

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);

                var department = '<tr id="department' + data.id + '"><td>' + data.id + '</td><td>' + data.task + '</td><td>' + data.created_at + '</td>';
                department += '<td><button class="btn btn-warning btn-xs btn-detail open-modal" value="' + data.id + '">Edit</button>';
                department += '<button class="btn btn-danger btn-xs btn-delete delete-department" value="' + data.id + '">Delete</button></td></tr>';

                if (state == "add"){ //if user added a new record
                    $('#departments-list').append(department);
                }else{ //if user updated an existing record

                    $("#department" + department_id).replaceWith( department );
                }

                $('#frmDepartments').trigger("reset");

                $('#myModal').modal('hide')
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});