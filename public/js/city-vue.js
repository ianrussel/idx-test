var vue = new Vue({
	http: {
		root: '/root',
		headers: {
			'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
		}
	},

	el: '#CityController',

	data: {
		newCity: {
			id: '',
			city: '',
			state_id: ''
		},
		success: false,

		edit: false
	},

	methods: {
		allCities: function() {
			this.$http.get('/idx-test/cities', function(data) {
				this.$set('cities', data)
			})
		},
		removeCity: function(id) {
			var ConfirmBox = confirm("Are you sure to delete?, this cannot be undone!")
			if(ConfirmBox) this.$http.delete('/idx-test/city-delete/' + id)

			this.allCities()
		    this.refresh
		},

		editCity: function(id) {
			var city = this.newCity

			this.newCity = {id: '', city: '', state: ''}
			this.$http.patch('/idx-test/cities/' + id, city, function(data) {
				console.log(data)
			})
			this.allCities()
			this.edit = false
		},

		showCity: function(id) {
			this.edit = true
			this.$http.get('/idx-test/edit/cities/' + id, function(data) {
				this.newCity.id = data.id
				this.newCity.city = data.city
				this.newCity.state = data.state
			})
		},

		AddNewCity: function() {
			// user input
			var city = this.newCity;

			//clear form
			
			this.newCity = {city: '', state: ''}

			//post data
			this.$http.post('/idx-test/add-new-city', city)

			//show success message
			self = this
			this.success = true

			setTimeout(function() {
				self.success = false
			}, 5000)

			//reload 
			this.allCities()
		}
	},

	computed: {
		validation: function() {
			return {
				city: !!this.newCity.city.trim(),
				state: !!this.newCity.state.trim()
			}
		},

		isValid: function() {
			var validation = this.validation
			return Object.keys(validation).every(function(key) {
				return validation[key]
			})
		}
	},

	ready: function() {
		this.allCities()
	}
});