-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: idx
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_id` int(10) unsigned NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_state_id_foreign` (`state_id`),
  CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (2,2,'Manila','john pratts','john pratts','2016-05-24 00:00:00','2016-05-24 00:00:00'),(3,1,'Quezon City','Camille Pratts','Camille Pratts','2016-05-24 00:00:00','2016-05-24 05:15:17'),(4,1,'Davao City','Wally Manalo','Wally Manalo','2016-05-11 05:00:00','2016-05-25 00:00:00'),(5,4,'Pateros City','Wally Gonzales','Wally Gonzales','2016-05-24 09:00:00','2016-05-25 00:00:00'),(6,6,'Malabon City','Don Pablo','Michael Requestas','2016-05-11 00:00:00','2016-05-25 00:00:00'),(7,7,'Manila City','Rommel Padilla','Robin Padilla','2016-05-02 00:00:00','2016-05-25 00:00:00'),(8,10,'Navotas City','Anton Diva','Vice Ganda','2016-05-25 00:00:00','2016-05-25 09:00:00'),(9,20,'Mangga City','ianrussel',NULL,'2016-05-27 02:44:48','2016-05-27 02:44:48'),(10,19,'Santol City','ianrussel',NULL,'2016-05-27 03:19:08','2016-05-27 03:19:08');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_05_24_140116_create_table_sections',1),('2016_05_24_141024_create_table_states',1),('2016_05_24_141339_create_table_cities',1),('2016_05_24_142435_create_table_year_level',1),('2016_05_24_142633_create_table_students',1),('2016_05_29_004239_entrust_setup_tables',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(2,2),(3,2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'create student','create student','has access to create new student facility',NULL,'2016-05-29 11:03:26','2016-05-29 11:03:26'),(2,'create city','create city','has access to create city facility',NULL,'2016-05-29 11:05:34','2016-05-29 11:05:34'),(3,'edit student','edit student','has access to edit student facility','ianrussel','2016-05-29 11:06:53','2016-05-29 11:06:53');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(4,1),(5,1),(2,2),(3,2),(4,2);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','Administrator','Has All Access',NULL,'2016-05-29 10:42:12','2016-05-29 10:42:12'),(2,'Encoder','Encoder','Encode, Update Student Records','ianrussel','2016-05-29 10:46:01','2016-05-29 10:46:01');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'Handsome','Totoy James','James Irving','2016-05-24 08:00:00','2016-05-24 11:00:00'),(2,'Good','Amari Stoud','James Harden','2016-05-25 06:00:00','2016-05-25 12:00:00'),(3,'Bravo','Aga James','Michael de Mesa','2016-05-25 10:00:00','2016-05-25 18:00:00'),(4,'Delicious','Peter Pan','John Meyer','2016-05-26 00:00:00','2016-05-27 00:00:00'),(5,'Beautiful','Jessy Mendiolo','Vic Manalo','2016-05-25 00:00:00','2016-05-25 07:00:00'),(6,'Pretty','John Evans','Emmy Lou','2016-05-25 08:00:00',NULL),(7,'Cute','ianrussel',NULL,'2016-05-27 03:01:01','2016-05-27 03:01:01'),(8,'Gorgeous','ianrussel',NULL,'2016-05-27 03:14:20','2016-05-27 03:14:20'),(9,'Saint Francis','ianrussel',NULL,'2016-05-28 13:44:35','2016-05-28 13:44:35'),(10,'Saint Paul','ianrussel',NULL,'2016-05-28 13:44:48','2016-05-28 13:44:48'),(11,'Saint Therese','ianrussel',NULL,'2016-05-28 13:45:17','2016-05-28 13:45:17'),(12,'Saint John','ianrussel',NULL,'2016-05-28 13:45:38','2016-05-28 13:45:38'),(13,'Saint Peter','ianrussel',NULL,'2016-05-28 13:45:52','2016-05-28 13:45:52'),(14,'Saint Luke','ianrussel',NULL,'2016-05-28 13:46:07','2016-05-28 13:46:07');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'California','Dr. Lazaro Hoeger','Dr. Lauriane Altenwerth','2016-05-24 16:23:54','2016-05-24 16:23:54'),(2,'Colorado','Yvonne Toy','Prof. Ryder Hilll III','2016-05-24 16:23:54','2016-05-24 16:23:54'),(3,'Mississippi','Noah Mayer Sr.','Prof. Trisha Will','2016-05-24 16:23:54','2016-05-24 16:23:54'),(4,'Rhode Island','Coy Dach','Dr. Rubie Herzog Jr.','2016-05-24 16:23:54','2016-05-24 16:23:54'),(5,'Minnesota','Curtis Miller','Gladyce Bednar PhD','2016-05-24 16:23:54','2016-05-24 16:23:54'),(6,'Connecticut','Mr. Skylar Turcotte PhD','Carolyn Watsica','2016-05-24 16:23:54','2016-05-24 16:23:54'),(7,'Utah','Cleveland Lemke','Jayme Nikolaus','2016-05-24 16:23:54','2016-05-24 16:23:54'),(8,'Washington','Miss Natalie Upton','Loren Daniel','2016-05-24 16:23:54','2016-05-24 16:23:54'),(9,'District of Columbia','Tanner Morar','Ahmed Gusikowski DDS','2016-05-24 16:23:54','2016-05-24 16:23:54'),(10,'Oklahoma','Mossie Medhurst','Mr. Berta Wiza','2016-05-24 16:23:54','2016-05-24 16:23:54'),(11,'Pennsylvania','Mrs. Ashleigh Collins','Ms. Alexandra Hirthe III','2016-05-24 16:23:54','2016-05-24 16:23:54'),(12,'Maine','Adriel Yundt','Angie Homenick','2016-05-24 16:23:54','2016-05-24 16:23:54'),(13,'District of Columbia','Miss Lou Hayes','Mavis Reichel','2016-05-24 16:23:54','2016-05-24 16:23:54'),(14,'Pennsylvania','Estella Ebert','Sean Ryan Sr.','2016-05-24 16:23:54','2016-05-24 16:23:54'),(15,'Florida','Mohammad Cartwright','Jaydon Runolfsson','2016-05-24 16:23:54','2016-05-24 16:23:54'),(16,'New Hampshire','Elbert Rodriguez','Hanna Waters','2016-05-24 16:23:54','2016-05-24 16:23:54'),(17,'Rhode Island','Prof. Enos Schuppe PhD','Jody Schinner MD','2016-05-24 16:23:54','2016-05-24 16:23:54'),(18,'Maryland','Clarissa Reinger','Laverna Altenwerth','2016-05-24 16:23:54','2016-05-24 16:23:54'),(19,'Pennsylvania','Roberto Rodriguez','Kameron Stroman','2016-05-24 16:23:54','2016-05-24 16:23:54'),(20,'Montana','Irving Gerhold','Monroe Blanda','2016-05-24 16:23:54','2016-05-24 16:23:54'),(21,'Monaco','ianrussel',NULL,'2016-05-27 14:42:20','2016-05-27 14:42:20'),(22,'Moscow','Mar',NULL,'2016-05-29 15:37:05','2016-05-29 15:37:05'),(23,'Montividad','Mar',NULL,'2016-05-29 15:46:19','2016-05-29 15:46:19'),(24,'Cassablanca','Mar',NULL,'2016-05-29 16:28:09','2016-05-29 16:28:09');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bday` date NOT NULL,
  `age` int(10) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `yearlevel_id` int(10) unsigned NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_city_id_foreign` (`city_id`),
  KEY `students_section_id_foreign` (`section_id`),
  KEY `students_yearlevel_id_foreign` (`yearlevel_id`),
  CONSTRAINT `students_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `students_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
  CONSTRAINT `students_yearlevel_id_foreign` FOREIGN KEY (`yearlevel_id`) REFERENCES `yearlevels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (17,'Jerry ','Yan','2000-11-06',15,'dadadsdasdasdasdadad',10,'1234','1234567','12345678901','ian.russel@yahoo.com',5,7,'ianrussel','ianrussel','2016-05-26 15:16:48','2016-05-28 21:04:04'),(18,'Fidel','Ramos','2000-11-06',15,'123 Rodrigo Street',8,'1234','1234567','12345678901','ian.russel@yahoo.com',6,6,'ianrussel','ianrussel','2016-05-26 15:22:24','2016-05-26 18:43:32'),(20,'Panfilo','Lacson','2001-01-13',15,'1234 Bahay Toro',8,'1234','1234567','12345678901','ian.russel@yahoo.com',6,6,'ianrussel',NULL,'2016-05-26 15:39:08','2016-05-26 15:39:08'),(23,'Aurora','Lansing','1978-05-01',38,'1234 Magsaysay Avenue',4,'1234','1234567','12345678901','ian.russel@yahoo.com',3,2,'ianrussel',NULL,'2016-05-26 18:18:57','2016-05-26 18:18:57'),(26,'James','Bond','1970-12-07',45,'123 Asawa Ni Pare Ayaw Hilabti Kay Walay Pan**',6,'2314','1234567','12345678901','you@yahoo.com',12,2,'ianrussel',NULL,'2016-05-28 21:05:57','2016-05-28 21:05:57'),(27,'Floyd','Mayweather','1980-05-05',36,'123 Asawa ni Mare Ayaw hilabti kay walay pan**',10,'1234','2134562','12345678901','me@yahoo.com',14,7,'ianrussel','Mar','2016-05-28 21:07:56','2016-05-29 15:35:43'),(28,'Nicole','Kidman','1900-05-10',116,'123 Asawa ni Mare Ayaw hilabti kay walay pan**',10,'1234','1234567','12345678901','ian.russel@yahoo.com',14,7,'Mar',NULL,'2016-05-29 16:06:50','2016-05-29 16:06:50');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ianrussel','ian.russel@yahoo.com','$2y$10$kVN/EHtIHZUSat4L1CZDeeviVaAlBNKdunGVv.gD5Ji0n0JOtIBOC',1,'jQUesyrO3fURzRYLjersCid2O2wdnrwsWii6hG0ZnmOJNBqFIP8vVLhCjW0b',NULL,'2016-05-25 20:06:55','2016-05-29 23:32:34'),(2,'john','john@yahoo.com','93dbc229b15534ce4e1eaf90ae618081',1,'VrYeNs399ntxhCC434Ld9CKiIhSxC82uUHtz0EfMtI7TD4FSQIiGERa2DtOs',NULL,'2016-05-29 09:47:35','2016-05-29 23:40:06'),(3,'Rodrigo','rodrigo@yahoo.com','$2y$10$V4H4yWY/PYRmY72bJeDnHO6XEh6UeropQblTHbtxV.7lQrK5RZOSy',1,NULL,NULL,'2016-05-29 14:46:04','2016-05-29 14:46:04'),(4,'Mar','mar@yahoo.com','$2y$10$90golAOrSh.Rxug52BQT5ebGhhQ1DenqMV9PpCTVJkNFzgdpvOc9m',1,'iZiH71NqIdPunX3gyKq21a7QhbnoLIAjIhtjz74pPLZx7H3Hel54Jdb7qH3H',NULL,'2016-05-29 15:27:56','2016-05-29 16:29:08'),(5,'juan','juan@yahoo.com','$2y$10$yGt8pqdw8WyhbDfMHxqndevnxdXVrzGfIKYRxXGNb8rWAGaVXJw5S',1,'rCOlVqxeAhrxCJfDC5PuV3z6dp4SheyuwUXUB02sxg7YWJ9W2YnjgyaZmotU',NULL,'2016-05-29 23:41:03','2016-05-29 23:52:33'),(6,'johnny','johnny@yahoo.com','$2y$10$GgnOuOtTVXcmddw4SroEluksClrNwQp0TGAnDY7tkoNb3QnfftMFK',0,'REgJGBszFAoSPBvBKya6Z9ncxSZWq3Jdbz4DWnFcoPhCP6R4af7A3pftCnB0',NULL,'2016-05-29 23:49:35','2016-05-30 00:04:29');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yearlevels`
--

DROP TABLE IF EXISTS `yearlevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yearlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `yearlevel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yearlevels`
--

LOCK TABLES `yearlevels` WRITE;
/*!40000 ALTER TABLE `yearlevels` DISABLE KEYS */;
INSERT INTO `yearlevels` VALUES (1,'First Year','Van Damme','Jet Li','2016-05-25 07:00:00','2016-05-25 15:00:00'),(2,'Second Year','Corazon Bullock',NULL,'2016-05-25 11:00:00','2016-05-25 17:00:00'),(3,'Third Year','Monsour del Sario','Jonathan Wage','2016-05-26 00:00:00','2016-05-27 00:00:00'),(4,'Fourth Year','Fabien Potencier','Taylor Otwel','2016-05-26 00:00:00','2016-05-28 00:00:00'),(5,'Fifth Year','Jeffrey Way','Simon Cowell','2016-05-25 00:00:00','2016-05-27 00:00:00'),(6,'Sixth Year','Tom Barillow','Stephen Seagal','2016-05-26 00:00:00','2016-05-28 00:00:00'),(7,'Sixth Year Level','ianrussel',NULL,'2016-05-27 14:23:22','2016-05-27 14:23:22');
/*!40000 ALTER TABLE `yearlevels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-30 11:30:25
